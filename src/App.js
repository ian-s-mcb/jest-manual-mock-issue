// src/App.js
import SomeComponent from './SomeComponent'

const App = () => (
  <div className="App">
    <SomeComponent />
  </div>
)

export default App
