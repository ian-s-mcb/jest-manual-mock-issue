// src/App.test.js
import { render, screen } from '@testing-library/react'

import App from './App'
import SomeComponent from './SomeComponent'

jest.mock('./SomeComponent')

beforeEach(() => {
  SomeComponent.mockClear() // Ensures that `calledTimes` is reset
})

it('should contain mocked mocked component', () => {
  render(<App />)
  screen.getByText(/mocked SomeComponent/i)
})

it('should have correct calledTimes', () => {
  render(<App />)
  screen.getByText(/mocked SomeComponent/i)
  expect(SomeComponent)
    .toHaveBeenCalledTimes(1)
})
