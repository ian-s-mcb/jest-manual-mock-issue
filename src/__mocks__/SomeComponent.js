// src/__mocks__/SomeComponent.js
const implementation = () => <p>mocked SomeComponent</p>
const SomeComponent = jest.fn().mockImplementation(implementation)
export default SomeComponent
